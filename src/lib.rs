// SPDX-License-Identifier: MIT

/// Implementation of https://github.com/mafintosh/nat-sampler in Rust
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Host(String);

impl From<&str> for Host {
    fn from(host: &str) -> Self {
        Self(host.into())
    }
}

impl From<String> for Host {
    fn from(host: String) -> Self {
        Self(host)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Port(u32);

impl From<u32> for Port {
    fn from(port: u32) -> Self {
        Self(port)
    }
}

#[derive(Debug, Clone)]
pub struct NatSampler {
    host: Option<Host>,
    port: Option<Port>,
    size: u32,
    a: Option<Rc<RefCell<NatSample>>>,
    b: Option<Rc<RefCell<NatSample>>>,
    threshold: u32,
    top: i32,
    samples: Vec<Rc<RefCell<NatSample>>>,
}

#[derive(Debug, Clone)]
struct NatSample {
    host: Host,
    port: Port,
    hits: u32,
}

impl NatSampler {
    pub fn new() -> Self {
        Self {
            host: None,
            port: None,
            size: 0,
            a: None,
            b: None,
            threshold: 0,
            top: 0,
            samples: vec![],
        }
    }

    pub fn host(&self) -> Option<Host> {
        self.host.clone()
    }

    pub fn port(&self) -> Option<Port> {
        self.port.clone()
    }

    pub fn add(&mut self, host: &Host, port: &Port) -> u32 {
        let a = self.bump(host, port, 2);
        let b = self.bump(host, &Port(0), 1);

        if self.samples.len() < 32 {
            self.size += 1;
            self.threshold = self.size
                - match self.size {
                    x if x < 4 => 0,
                    x if x < 8 => 1,
                    x if x < 12 => 2,
                    _ => 3,
                };
            self.samples.push(Rc::clone(&a));
            self.samples.push(Rc::clone(&b));
            self.top += 2;
        } else {
            if self.top == 32 {
                self.top = 0;
            }

            let orig_a = Rc::clone(&self.samples[self.top as usize]);
            self.samples[self.top as usize] = Rc::clone(&a);
            self.top += 1;
            orig_a.borrow_mut().hits -= 1;

            let orig_b = Rc::clone(&self.samples[self.top as usize]);
            self.samples[self.top as usize] = Rc::clone(&b);
            self.top += 1;
            orig_b.borrow_mut().hits -= 1;
        }

        self.a = match self.a {
            None => Some(Rc::clone(&a)),
            Some(ref sa) if sa.borrow().hits < (&a).borrow().hits => Some(Rc::clone(&a)),
            Some(ref sa) => Some(Rc::clone(&sa)),
        };
        self.b = match self.b {
            None => Some(Rc::clone(&b)),
            Some(ref sb) if sb.borrow().hits < (&b).borrow().hits => Some(Rc::clone(&b)),
            Some(ref sb) => Some(Rc::clone(&sb)),
        };

        // unwrap is safe here, since the previous lines ensure that a and b contain Some()
        let a_borrow = self.a.as_ref().unwrap().borrow();
        let b_borrow = self.b.as_ref().unwrap().borrow();
        if a_borrow.hits >= self.threshold {
            self.host = Some(a_borrow.host.clone());
            self.port = Some(a_borrow.port.clone());
        } else if b_borrow.hits >= self.threshold {
            self.host = Some(b_borrow.host.clone());
            self.port = None;
        } else {
            self.host = None;
            self.port = None;
        }

        let a_hits = a.borrow().hits;
        a_hits
    }

    fn bump(&mut self, host: &Host, port: &Port, inc: i32) -> Rc<RefCell<NatSample>> {
        for i in 0..4 {
            let j = (self.top - inc - (2 * i)) & 31;
            if j >= self.samples.len() as i32 {
                // return default value after the loop
                break;
            }
            let sample = &self.samples[j as usize];
            let mut sample_borrow = sample.borrow_mut();
            if &sample_borrow.port == port && &sample_borrow.host == host {
                sample_borrow.hits += 1;
                return Rc::clone(&sample);
            }
        }
        Rc::new(RefCell::new(NatSample {
            host: host.clone(),
            port: port.clone(),
            hits: 1,
        }))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn small_consistent_samples() {
        let mut nat = NatSampler::new();

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 1);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), Some(Port(9090)));

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 2);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), Some(Port(9090)));

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 3);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), Some(Port(9090)));
    }

    #[test]
    fn small_consistent_samples_with_errors() {
        let mut nat = NatSampler::new();

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 1);
        assert_eq!(nat.host(), Some(Host("127.0.0.1".into())));
        assert_eq!(nat.port(), Some(Port(9090)));

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9091)), 1);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), None);

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 2);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), None);

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 3);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), Some(Port(9090)));

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9091)), 2);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), None);
    }

    #[test]
    fn host_consistency() {
        let mut nat = NatSampler::new();

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 1);
        assert_eq!(nat.add(&"127.0.0.2".into(), &Port(9090)), 1);
        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 2);
        assert_eq!(nat.add(&"127.0.0.2".into(), &Port(9090)), 2);

        assert_eq!(nat.host(), None);
        assert_eq!(nat.port(), None);

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 3);
        assert_eq!(nat.host(), None);
        assert_eq!(nat.port(), None);

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 4);
        assert_eq!(nat.host(), None);
        assert_eq!(nat.port(), None);

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9090)), 5);
        assert_eq!(nat.host(), None);
        assert_eq!(nat.port(), None);

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9091)), 1);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), None);
    }

    #[test]
    fn can_recover_up_to_3_errors() {
        let mut nat = NatSampler::new();

        for _ in 0..20 {
            nat.add(&"127.0.0.1".into(), &Port(9090));
        }

        assert_eq!(nat.add(&"127.0.0.1".into(), &Port(9091)), 1);
        assert_eq!(nat.add(&"127.0.0.2".into(), &Port(9091)), 1);
        assert_eq!(nat.add(&"127.0.0.3".into(), &Port(9091)), 1);
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), Some(Port(9090)));

        for _ in 0..5 {
            nat.add(&"127.0.0.1".into(), &Port(9090));
        }
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), Some(Port(9090)));

        nat.add(&"127.0.0.1".into(), &Port(9095));
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), None);

        nat.add(&"127.0.0.2".into(), &Port(9095));
        assert_eq!(nat.host(), Some("127.0.0.1".into()));
        assert_eq!(nat.port(), None);

        nat.add(&"127.0.0.2".into(), &Port(9095));
        assert_eq!(nat.host(), None);
        assert_eq!(nat.port(), None);
    }
}
